// Translation provided by Vuetify (typescript)
import es from '@/locales/vuetify/es'

export default {
  breakpoint: {},
  icons: {},
  lang: {
    locales: { es },
    current: 'es'
  },
  rtl: false,
  theme: {}
}
