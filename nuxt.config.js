import bodyParser from 'body-parser'
import session from 'express-session'
import ConnectRedis from 'connect-redis'

const RedisStore = ConnectRedis(session)

export default {
  mode: 'universal',

  serverMiddleware: [
    bodyParser.json(),
    session({
      secret: process.env.SESSION_SECRET || 'secret',
      sameSite: true,
      resave: false,
      proxy: true,
      httpOnly: true,
      saveUninitialized: false,
      rolling: true,
      cookie: {
        maxAge: 365 * 24 * 60 * 60 * 1000
        // secure: !!process.env.USING_HTTPS,
        // domain: process.env.DOMAIN || 'myinventory.app'
      }, // one year
      store: new RedisStore({
        prefix: 'staff-rota:',
        host: process.env.REDIS_HOST || '127.0.0.1',
        port: process.env.REDIS_PORT || 6379
      })
    }),
    '~/server-middleware/auth'
  ],

  /*
   ** Headers of the page
   */
  head: {
    title: 'My Inventory App',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: 'Simple inventory, new generation'
      }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#c3ffc5' },
  /*
   ** Global CSS
   */
  css: ['@/assets/fix'],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: ['@/plugins/i18n', '@/plugins/axios.client'],

  router: {
    middleware: ['auth', 'protected', 'lang']
  },
  /*
   ** Nuxt.js dev-modules
   */
  devModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    '@nuxtjs/vuetify'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    'cookie-universal-nuxt',
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa'
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  vuetify: {
    defaultAssets: {
      font: true,
      icons: 'md'
    },
    optionsPath: '@/assets/vuetify.options.js'
  },
  /*
   ** Build configuration
   */
  build: {
    babel: {
      plugins: [
        [
          '@babel/plugin-proposal-decorators',
          {
            legacy: true
          }
        ],
        [
          '@babel/plugin-proposal-class-properties',
          {
            loose: true
          }
        ]
      ]
    },
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {}
  }
}
