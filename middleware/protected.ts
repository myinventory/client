export default function({ store: { state }, route, redirect }) {
  const whitelisted = ['/signup', '/forgot', '/recovery']

  for (const index in whitelisted) {
    if (route.path === whitelisted[index]) {
      return
    }
  }

  const hasToken = state.auth.token

  const pathProtected = /^\/app/.test(route.path)

  const homePage = route.path === '/'

  if (homePage && hasToken) {
    redirect('/app')
  }

  if (pathProtected && !hasToken) {
    redirect('/?login=1')
  }
}
