export default async function({ store: { dispatch, commit }, req, app }) {
  if (process.browser) {
    return
  }

  const token = req.session && req.session.token

  if (token) {
    app.$axios.defaults.baseURL = 'https://api.myinventory.app'
    app.$axios.setToken(token, 'Bearer')
    try {
      await dispatch('auth/fetchUser')
      commit('auth/setToken', token)
    } catch (e) {
      if (e.response && e.response.status === 401) {
        req.session.token = null
      } else {
        throw e
      }
    }
  }
}
