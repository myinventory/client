import express from 'express'
import axios from 'axios'

const SERVER_HOST = 'api.myinventory.app'
const CLIENT_ID = 2
const CLIENT_SECRET = 'JcvxKUNiae75ooxpJV71EIyMjrT43zMgxYsyHcF7' // 'kGU0RAORNbLn358NB4iiM3l101G4sFIoIrOVHemW'

axios.defaults.baseURL = `https://${SERVER_HOST}`

const handler = express()

handler.post('/login', async (request, response) => {
  try {
    const { data } = await axios.post('/oauth/token', {
      grant_type: 'password',
      client_id: CLIENT_ID,
      client_secret: CLIENT_SECRET,
      username: request.body.email.toLowerCase(),
      password: request.body.password
    })
    // @ts-ignore
    request.session.token = data.access_token
    response.json(data)
  } catch (e) {
    console.log(e)
    if (request.session) {
      request.session.token = null
    }
    if (e.response) {
      const { data, status } = e.response
      response.status(status).json(data)
    } else {
      response.status(500).json()
    }
  }
})

handler.post('/logout', async (req, res) => {
  try {
    const { data } = await axios.post('/api/logout', null, {
      headers: {
        authorization: req.header('authorization')
      }
    })
    // @ts-ignore
    req.session.destroy()
    res.status(200).json(data)
  } catch (e) {
    console.log(e)
    if (req.session) {
      // @ts-ignore
      req.session.destroy()
    }
    if (e.response) {
      const { data, status } = e.response
      res.status(status).json(data)
    } else {
      res.status(500).json()
    }
  }
})

module.exports = {
  path: '/auth',
  handler
}
