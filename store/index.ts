export const state = () => ({
  locale: 'es'
})

export const mutations = {
  setLocale(state, locale: string) {
    state.locale = locale
  }
}
