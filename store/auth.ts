const SERVER_HOST = 'https://api.myinventory.app'

export const state = () => {
  return {
    token: null,
    user: null
  }
}

export const mutations = {
  setUser(state, user) {
    state.user = user
  },
  setToken(state, token) {
    state.token = token
  }
}

export const getters = {
  connected: (state) => !!state.user
}

export const actions = {
  async fetchUser({ commit }) {
    const {
      data: { data }
      // @ts-ignore
    } = await this.$axios.get(`${SERVER_HOST}` + '/api/auth/me')
    commit('setUser', data)
  }
}
