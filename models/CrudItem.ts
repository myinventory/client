export type CrudItem = {
  id: number
  name: string
}
