import { TranslateResult } from '~/node_modules/vue-i18n'

export type SelectOption<T> = {
  text: TranslateResult
  value: T
}
