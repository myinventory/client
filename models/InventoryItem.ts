export type InventoryItem = {
  code: string
  timestamp: string
}
