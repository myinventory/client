export type HistoryItem = {
  code: string
  timestamp: string
}
