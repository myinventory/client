import Vue from 'vue'
import VueI18n from 'vue-i18n'
import VeeValidate from 'vee-validate'
import validateEn from 'vee-validate/dist/locale/en'
import validateEs from 'vee-validate/dist/locale/es'

Vue.use(VueI18n)

const i18n = new VueI18n({
  locale: 'en',
  fallbackLocale: 'en',
  silentTranslationWarn: true,
  messages: {
    en: require('@/locales/en/translations.json'),
    es: require('@/locales/es/translations.json')
  }
})

Vue.use(VeeValidate, {
  inject: false,
  i18nRootKey: 'validations', // customize the root path for validation messages.
  i18n,
  dictionary: {
    en: validateEn,
    es: validateEs
  }
})

export default ({ app, store }) => {
  app.i18n = i18n

  const lang = app.$cookies.get('lang') || 'en'

  app.i18n.locale = lang
  app.vuetify.framework.lang.current = lang
}
