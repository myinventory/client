export default function({ $axios, store }) {
  if (store.state.auth.token) {
    $axios.setToken(store.state.auth.token, 'Bearer')
  }

  const SERVER_HOST = 'api.myinventory.app'

  $axios.defaults.baseURL = `https://${SERVER_HOST}`
}
