export default {
  close: 'Cerrar',
  dataIterator: {
    pageText: '{0}-{1} de {2}',
    noResultsText: 'Ningún resultado a mostrar',
    loadingText: 'Cargando elemento...'
  },
  dataTable: {
    itemsPerPageText: 'Filas por página:',
    ariaLabel: {
      sortDescending: ': Ordenar descendente. Activar para remover orden.',
      sortAscending: ': Ordenar ascendente. Activar para ordenar descendente.',
      sortNone: ': Not sorted. Activate to sort ascending.'
    },
    sortBy: 'Ordenar por'
  },
  dataFooter: {
    itemsPerPageText: 'Elementos por página:',
    itemsPerPageAll: 'Todos',
    nextPage: 'Página siguiente',
    prevPage: 'Página anterior',
    firstPage: 'Página primera',
    lastPage: 'Página última'
  },
  datePicker: {
    itemsSelected: '{0} seleccionados'
  },
  noDataText: 'Ningún dato disponible',
  carousel: {
    prev: 'Visual previo',
    next: 'Siguiente visual'
  },
  calendar: {
    moreEvents: '{0} más'
  },
  fileInput: {
    counter: '{0} files',
    counterSize: '{0} files ({1} in total)'
  }
}
